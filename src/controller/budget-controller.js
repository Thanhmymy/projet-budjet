import { Router } from "express";
import { addBudget, deleteBudget, findBudget, findByDate, findOneBudget, updateBudget } from "../repository/budget-repository.";

export const budgetController = Router();

//Method par mois
budgetController.get('/month/:date', async(req, resp)=>{
    let budget = await findByDate(req.params.date)

    resp.json(budget)

})

//Method Add

budgetController.post('/add', async (req,res)=>{
    let id = await addBudget(req.body);
    res.status(201).json(await findOneBudget(id))
})

//Affichage de tout le tableau
budgetController.get('/api/budget/budgets',async (req,resp)=>{
    let budget = await findBudget();
    resp.json(budget)
});

//Affichage par id
budgetController.get('/one/:id', async (req,resp)=>{
    let budget = await findOneBudget(req.params.id);
    if(!budget){
        resp.status(404).json ({error : 'Pas trouvé'});
    return;
    }
    resp.json(budget)
})

//Update
budgetController.put('/update',async (req,resp)=>{
    await updateBudget(req.body);
    resp.end()
})

//Delete

budgetController.delete('/delete/:id',async (req,resp)=>{
    try{
        await deleteBudget(req.params.id);
        resp.status(204).end()
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }
})