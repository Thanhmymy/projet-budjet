import { Budget } from "../entity/Budget";
import { connection } from "./connection";

export async function findByDate(date){
    const [rows] = await connection.execute('SELECT * FROM budget WHERE MONTH(date)=?', [date]);

    const budget = [];
    for(const row of rows){
        let instance = new Budget(row.category, row.date, row.amount, row.id);
        budget.push(instance);
    }
    return budget;
}



export async function addBudget(budget){
    let [data]= await connection.query ('INSERT INTO budget (category,date,amount) VALUES (?,?,?)', [budget.category,budget.date,budget.amount]);
    return budget.id = data.insertId
}


export async function findBudget() {


    const [rows] = await connection.execute('SELECT * FROM budget');
    
    const budget = [];
    for (const row of rows) {
        let instance = new Budget(row.category, row.date, row.amount,row.id);
        budget.push(instance);

    }
    return budget;
}

/**
 * 
 * @param {Number} id 
 * @returns {Promise<Budget}
 */
export async function findOneBudget(id){
    const [rows] = await connection.query ('SELECT * FROM budget WHERE id= ?',[id]);
    if (rows.length === 1){
        return new Budget(rows[0].category,rows[0].date,rows[0].amount,rows[0].id)
    }
    return null;
}

export async function updateBudget(parBudget){
    const [rows] = await connection.query('UPDATE budget SET category=?, date=?, amount=? WHERE id=?', [parBudget.category, parBudget.date, parBudget.amount, parBudget.id])
}

export async function deleteBudget(id){
    const [rows] = await connection.query('DELETE FROM budget WHERE id=?', [id])
}