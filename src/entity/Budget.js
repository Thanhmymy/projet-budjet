export class Budget {
    id;
    category;
    date;
    amount;
    /**
     * 
     * @param {String} category 
     * @param {Date} date 
     * @param {Number} amount 
     * @param {Number} id 
     */
    constructor(category,date,amount,id=null){
        this.category = category;
        this.date = date;
        this.amount = amount;
        this.id = id;
    }
}