DROP TABLE IF EXISTS budget;
CREATE TABLE budget(
    id INT auto_increment primary key not null,
    category VARCHAR (255),
    date DATE,
    amount FLOAT
);

INSERT INTO budget (category,date,amount) VALUES ("Restaurant","2021-06-21", -56.68),
("Tabac","2021-06-20",-6.40),
("Shopping","2021-06-20",-26.90),
("Revenu","2021-06-07",+500);
