import { server } from "../src/server";
import request from "supertest";


describe ("Test",()=>{
    
    it ("It should return a table of budget",async()=>{
        let response = await request(server)
        .get('/api/budget/budgets')
        .expect(200);

        expect(response.body).toContainEqual({
            id: expect.any(Number),
            category : expect.any(String),
            date: expect.any(String),
            amount: expect.any(Number)
        });
    });
})